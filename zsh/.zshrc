# my environmental variables #########################
export PYTHONPATH="/home/azeez/.local/lib/python3.5/site-packages/"
export CARGOBIN="/home/azeez/.cargo/bin"
export PATH="${PATH}:${CARGOBIN}:/home/azeez/bin";
export export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export EDITOR='nvim'


# tmux ###############################################
if [ "$TMUX" = "" ]; then
    export PATH="${PATH}:/home/azeez/.gem/ruby/2.5.0/bin:/home/cmput274/.local/bin";
    exec tmux;
fi


# oh-my-zsh ##########################################
export ZSH="/home/azeez/.oh-my-zsh"
ZSH_THEME="common"

plugins=(thefuck git zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# starship ###########################################
eval "$(starship init zsh)"

# thefuck ############################################
eval $(thefuck --alias)

# my settings ########################################

COWPATH=/home/azeez/.cowsay/bkendzior/:/usr/share/cows
touch /home/azeez/floatingHead.txt
[ -f /home/azeez/floatingHead.txt ] && rm /home/azeez/floatingHead.txt && (fortune -s -n 400 | cowsay -W 40 -f rickNmortyHead) >> /home/azeez/floatingHead.txt && neofetch --backend ascii --source /home/azeez/floatingHead.txt
#unbuffer lolcat rickMortyFloatingHead.txt| tee floatingHead.txt >> /dev/null  && neofetch --backend ascii --source floatingHead.txt --help 
#fortune -s -n 150 | cowsay | lolcat


delcheck() {
  printf '\e[31mAttention: %d files will be permanently deleted:\n\e[0m' "$#"
  printf '"%s"\n' "$@"
  printf 'Are you sure want to delete them? [y/N] '
  read doit
  case "$doit" in
    [yY]) rm "$@" && printf '\e[31mFiles Deleted!\n\e[0m';;
    *) printf '\e[34mNo files deleted\n\e[0m';;
  esac
}

alias rm=delcheck
alias pbcopy="xclip -selection c" 
alias pbpaste="xclip -selection clipboard -o" 
alias anboxMan="anbox launch --package=org.anbox.appmgr --component=org.anbox.appmgr.AppViewActivity"
alias ls="ls -la --color"
alias l='colorls -l --sd --gs'
alias maim="maim -s /home/azeez/Pictures/screenshots/`echo $DATE`.png | xclip -selection clipboard -t image/png"
alias screenshot='maim -s --format jpg /dev/stdout | xclip -selection clipboard -t image/jpg -i'
alias godot=/home/azeez/Godot/3.0.4/Godot_v3.0.4-stable_x11.64
alias MG3=/home/azeez/gitRepos/MazeGame_G3.0_ported/
alias vim=nvim
alias gap="git add --patch ."

######################################################

BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

