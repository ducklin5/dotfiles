if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins ---------------------------------------------------------------------
call plug#begin('~/.vim/plugged')


Plug 'mhinz/vim-startify'

Plug 'chriskempson/base16-vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'joshdick/onedark.vim'
Plug 'baskerville/bubblegum'
Plug 'NLKNguyen/papercolor-theme'
Plug 'flrnd/candid.vim'
Plug 'qualiabyte/vim-colorstepper'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'edkolev/tmuxline.vim'

Plug 'dyng/ctrlsf.vim'

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'kshenoy/vim-signature'

Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

Plug 'majutsushi/tagbar'
Plug 'ludovicchabant/vim-gutentags'

Plug 'wfxr/minimap.vim', {'do': ':!cargo install --locked code-minimap'}

Plug 'Shougo/echodoc.vim'

Plug 'ctrlpvim/ctrlp.vim'

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

Plug 'neoclide/jsonc.vim'
Plug 'sheerun/vim-polyglot'
Plug 'calviken/vim-gdscript3'
Plug 'lepture/vim-jinja'
Plug 'lumiliet/vim-twig'
Plug 'cpiger/NeoDebug'

Plug 'alvan/vim-closetag'
Plug 'Raimondi/delimitMate'

Plug 'Chiel92/vim-autoformat'

Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}

call plug#end()

" Normal vim settings ---------------------------------------------------------
syntax on
set title
set laststatus=2
set cmdheight=2
set t_Co=256
set cursorline
set cursorcolumn
"set colorcolumn=79
set number
set splitbelow
set mouse=a
"set term=xterm-256color

set backspace=indent,eol,start

set smartindent
set shiftwidth=4 tabstop=4
set softtabstop=0 noexpandtab

set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2

set list
set listchars=tab:»\ ,eol:↪,extends:❯,precedes:❮,trail:·
" keep the list chars 'tranlucent' while editing a line
au VimEnter * call matchadd('SpecialKey', '^\s\+', -1) " tab
au VimEnter * call matchadd('SpecialKey', '\s\+$', -1) " whitespace
au VimEnter * call matchadd('SpecialKey', '\n', -1) " eol


if has('clipboard')
	if has('unnamedplus')  " When possible use + register for copy-paste
		set clipboard=unnamed,unnamedplus
	else         " On mac and Windows, use * register for copy-paste
		set clipboard=unnamed
	endif
endif

"--- BackupOnSave---
"Turn on backup option
set backup

set backupdir=~/.vim/backup/,/tmp//
set directory=~/.vim/swap/,/tmp//
set undodir=~/.vim/undo/,/tmp//

"Make backup before overwriting the current buffer
set writebackup

"Overwrite the original backup file
set backupcopy=yes

"Meaningful backup name, ex: filename@2015-04-05.14:59
au BufWritePre * let &bex = '@' . strftime("%F.%H")

"" file detect
filetype plugin indent on
autocmd BufRead,BufNewFile *.gd set filetype=gdscript3
autocmd BufRead,BufNewFile *.twig set filetype=jinja.html.twig

au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=4 sts=4 sw=4 expandtab

autocmd FileType json syntax match Comment +\/\/.\+$+
augroup python_files
	autocmd!
	autocmd FileType python setlocal expandtab
	autocmd FileType python set tabstop=4
	autocmd FileType python set shiftwidth=4
augroup END

"hi Normal ctermbg=none guibg=none
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

function! GetHiGroup(group)
	let result = execute('hi ' . 'CursorLineNr')
	let result = substitute(result, 'xxx',' ','')
	return substitute(result, '\n',' ', '')
endfunction

let myCursorLineNrHi = GetHiGroup('CursorLineNr')
augroup MyCursorLN
	autocmd!
	autocmd ColorScheme * let myCursorLineNrHi = GetHiGroup('CursorLineNr')
	autocmd InsertEnter * hi CursorLineNr ctermfg=green
	autocmd InsertLeave * execute('hi '.myCursorLineNrHi)
augroup END

" Plugin Options --------------------------------------------------------------
set background=dark
color PaperColor

" COC
call coc#config('python', {'pythonPath': $PYENV_VIRTUAL_ENV})


" UltiSnips
let g:UltiSnipsExpandTrigger="<c-k>"
let g:UltiSnipsJumpForwardTrigger="<c-k>"
let g:UltiSnipsJumpBackwardTrigger="<c-l>"
let g:UltiSnipsEditSplit="vertical"

"" Airline
let g:airline_powerline_fonts = 2
let g:airline_theme='tomorrow'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#coc#enabled = 1

"" Tmuxline
let g:tmuxline_preset = {
			\'a'    : '#S',
			\'c'    : ['#(whoami)', '#(uptime | cut -d " " -f 1,2,3)'],
			\'win'  : ['#I', '#W'],
			\'cwin' : ['#I', '#W', '#F'],
			\'y'    : ['%R', '%a', '%Y'],
			\'z'    : '#H'}

"" nerdTree
let NERDTreeShowHidden=1
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd BufWinEnter * silent NERDTreeMirror
let g:NERDTreeGitStatusIndicatorMapCustom = {
			\ "Modified"  : "✹",
			\ "Staged"    : "✚",
			\ "Untracked" : "✭",
			\ "Renamed"   : "➜",
			\ "Unmerged"  : "═",
			\ "Deleted"   : "✖",
			\ "Dirty"     : "✗",
			\ "Clean"     : "✔︎",
			\ 'Ignored'   : '☒',
			\ "Unknown"   : "?"
			\ }

"" Tagbar
let g:tagbar_type_css = {
\ 'ctagstype' : 'Css',
    \ 'kinds'     : [
        \ 'c:classes',
        \ 's:selectors',
        \ 'i:identities'
    \ ]
\ }

"" minimap
hi MinimapCurrentLine ctermfg=Blue ctermbg=18 guifg=#50FA7B guibg=#32302f
let g:minimap_highlight = 'MinimapCurrentLine'

"" autoformat
"let g:formatdef_ClangFormatGoogle = '"clang-format -style=Google"'
"let g:formatters_cpp = ['ClangFormatGoogle']

"" MatchTagAlways
let g:mta_filetypes = {
			\ 'html' : 1,
			\ 'xhtml' : 1,
			\ 'xml' : 1,
			\ 'jinja' : 1,
			\ 'jinja.html.twig': 1
			\}

"" neovim
let g:omni_sql_no_default_maps = 1

" Shortcut --------------------------------------------

"" Terminal sends Nul(C-@) instead of Ctrl-Space to vim
imap <C-@> <C-Space>

" Buffer
nnoremap gb :CocList buffers<CR>
nnoremap <C-x> :bd<CR>

" Tab navigation like Firefox.
noremap <C-t> :tabnew<CR>
noremap <C-u> :tabnext<CR>
noremap <C-y> :tabprevious<CR>

" {{{ Move windows between tabs
function MoveToPrevTab()
	"there is only one window
	if tabpagenr('$') == 1 && winnr('$') == 1
		return
	endif
	"preparing new window
	let l:tab_nr = tabpagenr('$')
	let l:cur_buf = bufnr('%')
	if tabpagenr() != 1
		close!
		if l:tab_nr == tabpagenr('$')
			tabprev
		endif
		sp
	else
		close!
		exe "0tabnew"
	endif
	"opening current buffer in new window
	exe "b".l:cur_buf
endfunc

function MoveToNextTab()
	"there is only one window
	if tabpagenr('$') == 1 && winnr('$') == 1
		return
	endif
	"preparing new window
	let l:tab_nr = tabpagenr('$')
	let l:cur_buf = bufnr('%')
	if tabpagenr() < tab_nr
		close!
		if l:tab_nr == tabpagenr('$')
			tabnext
		endif
		sp
	else
		close!
		tabnew
	endif
	"opening current buffer in new window
	exe "b".l:cur_buf
endfunc
" }}}

noremap <C-i> :call MoveToNextTab()<CR><C-w>H
noremap <C-o> :call MoveToPrevTab()<CR><C-w>H

" Windows
map <C-Up> <C-W>+
map <C-Down> <C-W>-
map <C-Right> <c-w>>
map <C-Left> <c-w><

" Misc.
nnoremap <CR> <esc>i
nnoremap <silent> <Esc><Esc> :noh<CR>
map <C-f> :NERDTreeToggle<CR>

" Use <c-space> to trigger completion.
imap <silent><expr> <c-space> coc#refresh()

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> gr <Plug>(coc-references)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> ge  :<C-u>CocList diagnostics<cr>
" Search workspace symbols.
nnoremap <silent><nowait> gs  :<C-u>CocList -I symbols<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

